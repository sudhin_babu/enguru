from common.models import User
from .models import Notification


class UsersToNotify:
    """
        pull records from database with 'DB_FETCH_LIMIT' no records
        at a time.
    """
    DB_FETCH_LIMIT = 20

    def __init__(self, notification_id):
        notification = Notification.objects.get(id=notification_id)
        self.notification = notification
        self.condition_dict = eval(notification.condition)
        self.DB_MAX_USERS = User.objects.filter(**self.condition_dict).count()
        self._len = self.DB_MAX_USERS - 1
        self.users = []

    def __iter__(self):
        self.index = 0
        self._array_index = 0
        self._offset_multiplier = 1
        return self

    def next(self):
        try:
            user = self.users[self._array_index]
            self._array_index += 1
            return user
        except IndexError:
            self._array_index = 0
            users_to_fetch = self.DB_MAX_USERS - self.index

            if users_to_fetch > 1:
                self.users = User.objects.filter(
                    **self.condition_dict)[self.index:self.DB_FETCH_LIMIT*self._offset_multiplier]
                self._offset_multiplier += 1
                user = self.users[self._array_index]
                self._array_index += 1
                return user
            else:
                raise StopIteration
        finally:
            self.index += 1

    def __len__(self):
        return self._len

    def __repr__(self):
        return "<UsersToNotify:{}> {} -- {} -- {}".format(self.notification.id,
                                                          self.notification.title,
                                                          self.notification.time,
                                                          self._len)
