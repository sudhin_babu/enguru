from django.core.management.base import BaseCommand
from common.models import User
from schedulednotification.models import UserNotificationLog


class Command(BaseCommand):
    help = 'delete all test users'

    def handle(self, *args, **options):
        print "total users = {}".format(User.objects.count())
        print "user notification log count = {}".format(UserNotificationLog.objects.count())
