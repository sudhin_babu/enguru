from django.core.management.base import BaseCommand
from common.models import User
import random


class Command(BaseCommand):
    help = 'Create test users'
    names = ['john', 'cersei', 'daenerys']
    category = ['basic', 'standard', 'premium']

    def add_arguments(self, parser):
        parser.add_argument('count', type=int)

    def handle(self, *args, **options):
        count = options['count']

        try:
            latest_id = User.objects.latest().id
            start = latest_id+1
            end = start + count
        except User.DoesNotExist:
            start = 0
            end = count

        for i in range(start, end):
            name = "{}{}".format(random.choice(self.names), i)
            email = "{}@mailinator.com".format(name)
            gcd_id = "gcd{}".format(name)
            cat = random.choice(self.category)
            User.objects.create(first_name=name,
                                username=name,
                                email=email,
                                gcd_id=gcd_id,
                                category=cat)
        print "total users = {}".format(User.objects.count())
