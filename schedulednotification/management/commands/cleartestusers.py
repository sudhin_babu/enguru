from django.core.management.base import BaseCommand
from common.models import User


class Command(BaseCommand):
    help = 'delete all test users'

    def handle(self, *args, **options):
        User.objects.all().delete()
