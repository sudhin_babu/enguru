# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from common.models import User


class Notification(models.Model):
    title = models.CharField(max_length=250)
    body = models.TextField()
    time = models.DateTimeField()
    condition = models.TextField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return "{} -- {}".format(self.title, self.time)


class UserNotificationLog(models.Model):
    user = models.ForeignKey(User)
    notification = models.ForeignKey(Notification)
    sent = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
