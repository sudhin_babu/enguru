from rest_framework import serializers
from common.models import User
from schedulednotification.models import Notification
from django.utils import timezone


class NotificationSerializer(serializers.Serializer):
    id = serializers.ReadOnlyField()
    title = serializers.CharField(max_length=250)
    body = serializers.CharField()
    condition = serializers.CharField(required=False)
    time = serializers.DateTimeField()

    def create(self, validated_data):
        notification = Notification.objects.create(**validated_data)
        # ntime = dateutilparser.parse(notification.time)
        return notification

    def validate_condition(self, condition):
        # eg: category__in=['basic']
        # eg: category__in=['permium', 'standard']
        if condition:
            try:
                dikt = eval(condition)
                User.objects.filter(**dikt).exists()
            except Exception as E:
                raise serializers.ValidationError(E.message)
        self.condition = condition
        return condition

    def update(self, notification, validated_data):
        for key, val in validated_data.items():
            setattr(notification, key, val)
        notification.save()
        return notification

    def validate_time(self, time):
        if time <= timezone.now():
            raise serializers.ValidationError("must greater than current time")
        return time
