import threading
from celery import Celery
from celery.utils.log import get_task_logger
# from django.core.mail import send_mail
from userstonotify import UsersToNotify
from .models import UserNotificationLog
import time


app = Celery('tasks', backend='amqp', broker='pyamqp://guest@localhost//')
logger = get_task_logger(__name__)


@app.task
def add_to_db(user, notification):
    UserNotificationLog.objects.create(user=user, notification=notification, sent=True)
    print "addded to db"


def send_user_notification(user, notification):
    msg = "Sending.. notfication for: {}".format(user.email)
    # print msg
    logger.info(msg)
    # send_mail(subject=notification.title,
    #           message=notification.body,
    #           from_email="johnsnow@gmail.com",
    #           recipient_list=[user.email])
    time.sleep(2)
    msg = "Finished.. notfication for: {}".format(user.email)
    logger.info(msg)

    # increase rate limit to work more
    # add_to_db.apply_async(args=(user, notification), serializer='pickle', rate_limit='2/s')
    add_to_db.apply(args=(user, notification), serializer='pickle')


@app.task
def notifyusers(notification_id):
    """
    send "group_max" no of notifcations in parallel
    """
    users = UsersToNotify(notification_id)
    notification = users.notification

    group = []
    group_len = 0
    group_max = 10000

    # start = time.time()
    for user in users:
        if group_len < group_max:
            thread = threading.Thread(target=send_user_notification,
                                      args=(user, notification))
            group.append(thread)
            thread.start()
            group_len += 1
        else:
            # print "group max---"
            for thread in group:
                thread.join()
            group = []
            group_len = 0

    # print "time taken -- {}".format(time.time() - start)
