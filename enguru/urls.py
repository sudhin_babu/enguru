from django.conf.urls import url
from django.contrib import admin
from schedulednotification.api import views
from django.views.generic import RedirectView
from django.core.urlresolvers import reverse_lazy

urlpatterns = [
    url(r'^$', RedirectView.as_view(url=reverse_lazy('notifications'), permanent=False)),
    url(r'^notifications/$', views.NotificationList.as_view(), name="notifications"),
    url(r'^notifications/(?P<pk>[0-9]+)/$', views.NotificationDetail.as_view()),
    url(r'^admin/', admin.site.urls),

]
