# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-19 04:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='category',
            field=models.CharField(choices=[('basic', 'basic'), ('standard', 'standard'), ('premium', 'premium')], default='basic', max_length=8),
            preserve_default=False,
        ),
    ]
