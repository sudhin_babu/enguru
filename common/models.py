# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User as AuthUser


class User(AuthUser):
    gcd_id = models.CharField(max_length=250)
    send_notifications = models.BooleanField(default=True)
    category = models.CharField(choices=[("basic", "basic"),
                                         ("standard", "standard"),
                                         ("premium", "premium")],
                                max_length=8)

    class Meta:
        get_latest_by = 'id'
